var isRunning = false;

const playText = "\u25B6";
const stopText = "\u25A0";
var runButton = document.getElementById('runButton');

var nextState = [];
var cellElements = [];
var neighborColors = [];

var colorPicker = document.getElementById("html5colorpicker");
var colorTextButton = document.getElementById("colorTextButton");

var mutationNumerator = document.getElementById("mutationNumerator");
var isMutationNumeratorChanging = false;
var mutationNumeratorPressed = false;
var mutationNumeratorValue = 0;

var mutationDenominator = document.getElementById("mutationDenominator");
var isMutationDenominatorChanging = false;
var mutationDenominatorPressed = false;
var mutationDenominatorValue = 1;

var diseaseNumerator = document.getElementById("diseaseNumerator");
var isDiseaseNumeratorChanging = false;
var diseaseNumeratorPressed = false;
var diseaseNumeratorValue = 0;

var diseaseDenominator = document.getElementById("diseaseDenominator");
var isDiseaseDenominatorChanging = false;
var diseaseDenominatorPressed = false;
var diseaseDenominatorValue = 1;

var startButton = document.getElementById("start_button");
var board = document.getElementsByClassName("board")[0];

var GENERATION = 0;
var generation = document.getElementById("generation-display");

var SPEED = 200;
var speed = document.getElementById("speed");
var speedPressed = false;
var isSpeedChanging = false;

var GRID_SIZE = 60;
var gridsize = document.getElementById("gridsize");
var gridsizePressed = false;
var isGridChanging = false;


var func;

init();

function init()
{
    colorPicker.addEventListener("change", function()
    {
        modifyCSS('div.cell:hover', 'background', colorPicker.value);
        colorTextButton.style.color = colorPicker.value;
    });
    
    modifyCSS('div.cell:hover', 'background', colorPicker.value);

    runButton.innerHTML = playText;

    mutationNumerator.innerHTML = 0;
    mutationDenominator.innerHTML = 1;

    diseaseNumerator.innerHTML = 0;
    diseaseDenominator.innerHTML = 1;
    
    showGeneration();
    speed.innerHTML = SPEED.toString();
    showGridSize();
}

function modifyCSS(className, element, value)
{
    if(!document.getElementById('customStyleSheet'))
    {
        var css = 'div.cell:hover { background: #FFFFFF; }'
        var style = document.createElement('style');

        style.id = 'customStyleSheet';

        if(style.styleSheet)
            style.styleSheet.cssText = css;
        else
            style.appendChild(document.createTextNode(css));

        document.getElementsByTagName('head')[0].appendChild(style);
    }

    var cssRuleCode = document.all ? 'rules' : 'cssRules';
    
    for(var i = 0; i < document.styleSheets.length; i++)
    {
        if(document.styleSheets[i][cssRuleCode] != null)
        {
            for(var j = 0; j < document.styleSheets[i][cssRuleCode].length; j++)
            {
                if(document.styleSheets[i][cssRuleCode][j].selectorText == className && 
                   document.styleSheets[i][cssRuleCode][j].style[element])
                {
                    document.styleSheets[i][cssRuleCode][j].style[element] = value;
                    return;
                }
            
            }
        }
    }

    console.log('Unable to find ' + className + ' with element ' + element + ' in style sheets. Value was not set to ' + value + '.');
}

function showGeneration()
{
    var div = document.getElementById("generation-display");
    div.innerHTML = "Generation: " + GENERATION.toString();
}

function updateGeneration()
{
    GENERATION++;
    showGeneration();
}

function showGridSize()
{
    gridsize.innerHTML = GRID_SIZE.toString();
    updateGrid();
}

function updateGrid()
{
    GRID_SIZE = parseInt(gridsize.innerHTML);
    board.style.setProperty("grid-template-columns", `repeat(${Math.floor(GRID_SIZE)}, 1fr)`);
    board.style.setProperty("grid-template-rows", `repeat(${Math.floor(GRID_SIZE)}, 1fr)`);
    
    removeChildren(board);
        
    cellElements.splice(0, cellElements.length);
    nextState.splice(0, cellElements.length);
    
	for(var i = 0; i < GRID_SIZE*GRID_SIZE; i++) nextState[i] = 0;
    
    for(var i = 0; i < GRID_SIZE*GRID_SIZE; i++)
    {
        var element = document.createElement("div");
        element.onclick = handleClick;
        element.IsCellOn = false;
        element.Changed = false;
        element.dataset.cellNumber = i;
        element.classList.add("cell");
        
        cellElements[i] = element;
        board.appendChild(element);
    }
}

function changeSpeed(additional)
{
    if((SPEED == 0 && additional == -1) ||
       (SPEED == 3000 && additional == 1) ||
       isRunning)
        return;
        
    SPEED += additional;
    speed.innerHTML = SPEED.toString();
            
    isSpeedChanging = true;
    
    setTimeout(function() 
    {  
        if(!isSpeedChanging)
            return;
            
        if(!speedPressed)
            speedPressed = setInterval(function()
            {
                if((SPEED == 0 && additional == -1) ||
                (SPEED == 3000 && additional == 1))
                {
                    clearInterval(speedPressed);
                    isSpeedChanging = false;
                    return;
                }
                
                SPEED += additional;
                speed.innerHTML = SPEED.toString();
            }, 50);
    }, 500);
}

function changeGridSize(additional)
{
    if((GRID_SIZE == 4 && additional == -1) || 
       (GRID_SIZE == 100 && additional == 1) ||
       isRunning)
        return;
        
    GRID_SIZE += additional;
    gridsize.innerHTML = GRID_SIZE.toString();
    
    isGridChanging = true;
    
    setTimeout(function() 
    {  
        if(!isGridChanging)
            return;
            
        if(!gridsizePressed)
            gridsizePressed = setInterval(function()
            {
                if((GRID_SIZE == 4 && additional == -1) || 
                (GRID_SIZE == 100 && additional == 1))
                {
                    clearInterval(gridsizePressed);
                    isGridChanging = false;
                    showGridSize();
                    return;
                }
                
                GRID_SIZE += additional;
                gridsize.innerHTML = GRID_SIZE.toString();
            }, 50);
    }, 500);
}

function changeMutationNumerator(additional)
{
    if((mutationNumeratorValue == 0 && additional == -1) ||
       (mutationNumeratorValue == 100 && additional == 1) ||
       (mutationNumeratorValue == mutationDenominatorValue && additional == 1) ||
       isRunning)
        return;

    mutationNumeratorValue += additional;
    mutationNumerator.innerHTML = mutationNumeratorValue;

    isMutationNumeratorChanging = true;

    setTimeout(function()
    {
        if(!isMutationNumeratorChanging)
            return;

        if(!mutationNumeratorPressed)
            mutationNumeratorPressed = setInterval(function()
            {
                if((mutationNumeratorValue == 0 && additional == -1) ||
                   (mutationNumeratorValue == 100 && additional == 1) ||
                   (mutationNumeratorValue == mutationDenominatorValue && additional == 1))
                {
                    clearInterval(mutationNumeratorPressed);
                    isMutationNumeratorChanging = false;
                    mutationNumerator.innerHTML = mutationNumeratorValue.toString();
                    return;
                }

                mutationNumeratorValue += additional;
                mutationNumerator.innerHTML = mutationNumeratorValue.toString();
            }, 50);
    }, 500);
}

function changeMutationDenominator(additional)
{
    if((mutationDenominatorValue == 1 && additional == -1) ||
       (mutationDenominatorValue == 100 && additional == 1) ||
       (mutationDenominatorValue == mutationNumeratorValue && additional == -1) || 
       isRunning)
        return;

    mutationDenominatorValue += additional;
    mutationDenominator.innerHTML = mutationDenominatorValue;

    isMutationDenominatorChanging = true;

    setTimeout(function()
    {
        if(!isMutationDenominatorChanging)
            return;

        if(!mutationDenominatorPressed)
            mutationDenominatorPressed = setInterval(function()
            {
                if((mutationDenominatorValue == 1 && additional == -1) ||
                   (mutationDenominatorValue == 100 && additional == 1) ||
                   (mutationDenominatorValue == mutationNumeratorValue && additional == -1))
                {
                    clearInterval(mutationDenominatorPressed);
                    isMutationDenominatorChanging = false;
                    mutationDenominator.innerHTML = mutationDenominatorValue.toString();
                    return;
                }

                mutationDenominatorValue += additional;
                mutationDenominator.innerHTML = mutationDenominatorValue.toString();
            }, 50);
    }, 500);
}

function changeDiseaseNumerator(additional)
{
    if((diseaseNumeratorValue == 0 && additional == -1) ||
       (diseaseNumeratorValue == 100 && additional == 1) ||
       (diseaseNumeratorValue == diseaseDenominatorValue && additional == 1) || 
       isRunning)
        return;

    diseaseNumeratorValue += additional;
    diseaseNumerator.innerHTML = diseaseNumeratorValue;

    isDiseaseNumeratorChanging = true;

    setTimeout(function()
    {
        if(!isDiseaseNumeratorChanging)
            return;

        if(!diseaseNumeratorPressed)
            diseaseNumeratorPressed = setInterval(function()
            {
                if((diseaseNumeratorValue == 0 && additional == -1) ||
                   (diseaseNumeratorValue == 100 && additional == 1) ||
                   (diseaseNumeratorValue == diseaseDenominatorValue && additional == 1))
                {
                    clearInterval(diseaseNumeratorPressed);
                    isDiseaseNumeratorChanging = false;
                    diseaseNumerator.innerHTML = diseaseNumeratorValue.toString();
                    return;
                }

                diseaseNumeratorValue += additional;
                diseaseNumerator.innerHTML = diseaseNumeratorValue.toString();
            }, 50);
    }, 500);
}

function changeDiseaseDenominator(additional)
{
    if((diseaseDenominatorValue == 1 && additional == -1) ||
       (diseaseDenominatorValue == 100 && additional == 1) ||
       (diseaseDenominatorValue == diseaseNumeratorValue && additional == -1) ||
       isRunning)
        return;

    diseaseDenominatorValue += additional;
    diseaseDenominator.innerHTML = diseaseDenominatorValue;

    isDiseaseDenominatorChanging = true;

    setTimeout(function()
    {
        if(!isDiseaseDenominatorChanging)
            return;

        if(!diseaseDenominatorPressed)
            diseaseDenominatorPressed = setInterval(function()
            {
                if((diseaseDenominatorValue == 1 && additional == -1) ||
                   (diseaseDenominatorValue == 100 && additional == 1) ||
                   (diseaseDenominatorValue == diseaseNumeratorValue && additional == -1))
                {
                    clearInterval(diseaseDenominatorPressed);
                    isDiseaseDenominatorChanging = false;
                    diseaseDenominator.innerHTML = diseaseDenominatorValue.toString();
                    return;
                }

                diseaseDenominatorValue += additional;
                diseaseDenominator.innerHTML = diseaseDenominatorValue.toString();
            }, 50);
    }, 500);
}

function toggleRun()
{
	if(isRunning)
        stop();
	else
		start();
}

function start()
{
	if(!isRunning)
	{
		isRunning = true;
        runButton.innerHTML = stopText;
		func = setInterval(run, parseInt(speed.innerHTML));
	}
}

function stop()
{
	isRunning = false;
    runButton.innerHTML = playText;
	clearInterval(func);
}

function reset()
{
    isRunning = false;
	GENERATION = 0;
	
	for(var i = 0; i < GRID_SIZE*GRID_SIZE; i++)
	{
		cellElements[i].classList.remove("on");
		cellElements[i].IsCellOn = false;
		cellElements[i].style.backgroundColor = "";
		nextState[i] = false;
	}
    
    showGeneration();
}

function run()
{
    var newColors = [];
    var changed = false;
    
    for(var i = 0; i < cellElements.length; i++)
    {
        var neighborCount = getNeighbors(i);
        
        if((neighborCount < 2 || neighborCount > 3) && 
           nextState[i] == 1)
        {
            changed = true;
            nextState[i] = 0;
            newColors.push("");
        }
        else if(neighborCount === 3)
        {
            if(nextState[i] == 0)
                changed = true;
            nextState[i] = 1;
            newColors.push(getAverageOfParentsColor(i));
        }
        else
        {
            newColors.push(cellElements[i].style.backgroundColor);
        }
    }
    
    if(!changed)
    {
        stop();
        return;
    }
    
    for(var i = 0; i < cellElements.length; i++)
    {
        setCell(i, newColors[i], ((nextState[i] == 1) && (Math.random() > (diseaseNumeratorValue / diseaseDenominatorValue))));
    }
    
    updateGeneration();
}

function clearIntervals()
{
    if(isSpeedChanging)
    {
        clearInterval(speedPressed);
        isSpeedChanging = false;
        speedPressed = false;
    }
    if(isGridChanging)
    {
        showGridSize();
        clearInterval(gridsizePressed);
        isGridChanging = false;
        gridsizePressed = false;
    }
    if(isMutationNumeratorChanging)
    {
        clearInterval(mutationNumeratorPressed);
        isMutationNumeratorChanging = false;
        mutationNumeratorPressed = false;
    }
    if(isMutationDenominatorChanging)
    {
        clearInterval(mutationDenominatorPressed);
        isMutationDenominatorChanging = false;
        mutationDenominatorPressed = false;
    }
    if(isDiseaseNumeratorChanging)
    {
        clearInterval(diseaseNumeratorPressed);
        isDiseaseNumeratorChanging = false;
        diseaseNumeratorPressed = false;
    }
    if(isDiseaseDenominatorChanging)
    {
        clearInterval(diseaseDenominatorPressed);
        isDiseaseDenominatorChanging = false;
        diseaseDenominatorPressed = false;
    }
}

function removeChildren(element)
{
    while(element.firstChild)
        element.removeChild(element.firstChild);
}

function getNeighbors(i)
{
    var neighbors = 0;
    
    try
    {
    if(i % GRID_SIZE != 0 && // not leftmost
    i >= GRID_SIZE && // not top row
    cellElements[i - GRID_SIZE - 1].IsCellOn) // upper left
        neighbors++;
    if(i % GRID_SIZE != 0 && // not leftmost
    cellElements[i - 1].IsCellOn) // left
        neighbors++;
    if(i % GRID_SIZE != 0 && // not leftmost
    i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) && // not bottom row
    cellElements[i + GRID_SIZE - 1].IsCellOn) // lower left
        neighbors++;
    if(i >= GRID_SIZE && // not top row
    cellElements[i - GRID_SIZE].IsCellOn) // directly above
        neighbors++;
    if(i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) && // not bottom row
    cellElements[i + GRID_SIZE].IsCellOn) // directly below
        neighbors++;
    if(i % GRID_SIZE != (GRID_SIZE - 1) && // not rightmost
    i >= GRID_SIZE && // not top row
    cellElements[i - GRID_SIZE + 1].IsCellOn) // upper right
        neighbors++;
    if(i % GRID_SIZE != (GRID_SIZE - 1) && // not rightmost
    cellElements[i + 1].IsCellOn) // right
        neighbors++;
    if(i % GRID_SIZE != (GRID_SIZE - 1) && // not rightmost
    i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) && // not bottom
    cellElements[i + GRID_SIZE + 1].IsCellOn) // lower right
        neighbors++;
    }
    catch(err)
    {
        console.log(i);
        console.log(Object.keys(cellElements[i]));
        console.log(cellElements[i]);
        console.log(cellElements[i].classList);
        console.log(Object.keys(cellElements[i].classList));
    }
    
    return neighbors;
}

function getAverageOfParentsColor(i)
{
	neighborColors = [];
	
    if(i % GRID_SIZE != 0 &&
       i >= (GRID_SIZE + 1) &&
       cellElements[i - GRID_SIZE - 1].IsCellOn) // upper left
    {
        if(cellElements[i - GRID_SIZE - 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i - GRID_SIZE - 1].style.backgroundColor);
    }
    if(i % GRID_SIZE != 0 &&
       cellElements[i - 1].IsCellOn) // left
    {
        if(cellElements[i - 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i - 1].style.backgroundColor);
    }
    if(i % GRID_SIZE != 0 &&
       i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) &&
       cellElements[i + GRID_SIZE - 1].IsCellOn) // lower left
    {
        if(cellElements[i + GRID_SIZE - 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i + GRID_SIZE - 1].style.backgroundColor);
    }
    if(i >= GRID_SIZE &&
       cellElements[i - GRID_SIZE].IsCellOn) // directly above
    {
        if(cellElements[i - GRID_SIZE].style.backgroundColor != "")
            neighborColors.push(cellElements[i - GRID_SIZE].style.backgroundColor);
    }
    if(i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) &&
       cellElements[i + GRID_SIZE].IsCellOn) // directly below
    {
        if(cellElements[i + GRID_SIZE].style.backgroundColor != "")
            neighborColors.push(cellElements[i + GRID_SIZE].style.backgroundColor);
    }
    if(i % GRID_SIZE != (GRID_SIZE - 1) &&
       i >= GRID_SIZE &&
       cellElements[i - GRID_SIZE + 1].IsCellOn) // upper right
    {
        if(cellElements[i - GRID_SIZE + 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i - GRID_SIZE + 1].style.backgroundColor);
    }
    if(i % GRID_SIZE != (GRID_SIZE - 1) &&
       cellElements[i + 1].IsCellOn) // right
    {
        if(cellElements[i + 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i + 1].style.backgroundColor);
    }
    if(i % GRID_SIZE != (GRID_SIZE - 1) &&
       i < (GRID_SIZE*GRID_SIZE - GRID_SIZE) &&
       cellElements[i + GRID_SIZE + 1].IsCellOn) // lower right
    {
        if(cellElements[i + GRID_SIZE + 1].style.backgroundColor != "")
            neighborColors.push(cellElements[i + GRID_SIZE + 1].style.backgroundColor);
    }
    
    return average(neighborColors);
}

function average(colors)
{
	var c = "#";
	
	var red = 0;
	var green = 0;
	var blue = 0;
    
    if(Math.random() < mutationNumeratorValue / mutationDenominatorValue)
    {
        red = Math.random()*255;
        green = Math.random()*255;
        blue = Math.random()*255;
    }
	else
    {
        var colorCount = 0;
        
        for(var i = 0; i < colors.length; i++)
        {
            var color = rgb2hex(colors[i]);
            if(color != "")
            {
                colorCount++;
                red += parseInt(color.substr(1, 2), 16);
                green += parseInt(color.substr(3, 2), 16);
                blue += parseInt(color.substr(5, 2), 16);
                
                if(isNaN(red))
                    red = 0;
                if(isNaN(green))
                    green = 0;
                if(isNaN(blue))
                    blue = 0;
            }
        }
        
        if(colorCount === 0)
            return "";
        
        red = isNaN(red) ? 0 : Math.floor(red / colorCount);
        green = isNaN(green) ? 0 : Math.floor(green / colorCount);
        blue = isNaN(blue) ? 0 : Math.floor(blue / colorCount);
    }
	
	var redStr = red.toString(16).toUpperCase();
	var greenStr = green.toString(16).toUpperCase();
	var blueStr = blue.toString(16).toUpperCase();
	
	redStr = ("0" + redStr).slice(-2);
	greenStr = ("0" + greenStr).slice(-2);
	blueStr = ("0" + blueStr).slice(-2);
	
	c += redStr + greenStr + blueStr;
	
    return c;
}

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

//Function to convert rgb color to hex format
function rgb2hex(rgb) 
{
    if(rgb.substring(0,1) === "#" ||
       rgb === "")
        return rgb;
        
    var hexVal = "#";
    var rx = /(.*?)rgb\((\d+), (\d+), (\d+)\)/;
    
    var digits = rx.exec(rgb);
    rx.lastIndex = 0;
    
    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);
    
    hexVal += hex(red) + hex(green) + hex(blue);
    
    return hexVal;
}

function hex(x) 
{
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function handleClick(event)
{
    if(!isRunning)
    {
        var cellNumber = parseInt(event.currentTarget.dataset.cellNumber);
        flipCell(cellNumber);
    }
}

function flipCell(cellNumber)
{
	if(cellNumber < 0 || cellNumber >= cellElements.length) return;
	cellElements[cellNumber].IsCellOn = !cellElements[cellNumber].IsCellOn;
        
    if(cellElements[cellNumber].IsCellOn)
	{
        cellElements[cellNumber].style.backgroundColor = document.getElementById("html5colorpicker").value;
		nextState[cellNumber] = 1;
	}
    else
	{
        cellElements[cellNumber].style.backgroundColor = "";
		nextState[cellNumber] = 0;
	}
}

function setCell(cellNumber, color, isCellOn)
{
	if(cellNumber < 0 || cellNumber >= cellElements.length) return;
	cellElements[cellNumber].IsCellOn = isCellOn;
	cellElements[cellNumber].style.backgroundColor = color;
}

function changeSeedingColor()
{
    if(!isRunning)
        colorPicker.click();
}